/*
 * funciones.c
 *
 *  Created on: 20/3/2015
 *      Author: Damian Rivera Honores
 */
#include "cabecera.h"
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <stdio.h>


int cargarPartida(short int unsigned mm[7][7], short int unsigned mj[7][7])
{
   short int unsigned i, j;
   FILE *f_carg;

   if (!(f_carg = fopen("guardar.dat", "rb")))
   {
      printf("Error en la apertura del fichero\n");
      return 1;
   }

   for(i=0;i<7;i++)
      for(j=0;j<7;j++)
      {
         fread(&mm[i][j], sizeof(mm[0][0]), 1, f_carg);
         fread(&mj[i][j], sizeof(mj[0][0]), 1, f_carg);
      }

   fclose(f_carg);
   return 0;
}
int guardarPartida(short int unsigned mm[7][7], short int unsigned mj[7][7])
{
   short int unsigned i, j;
   FILE *f_guar;

   if (!(f_guar = fopen("guardar.dat", "wb")))
   {
     printf("Error en la apertura del fichero\n");
     return 1;
   }
   for(i=0;i<7;i++)
      for(j=0;j<7;j++)
      {
         fwrite(&mm[i][j], sizeof(mm[0][0]), 1, f_guar);
         fwrite(&mj[i][j], sizeof(mj[0][0]), 1, f_guar);
      }

   fclose(f_guar);
   return 0;
}

short int unsigned juego(short int unsigned mm[7][7],short int unsigned mj[7][7],short int unsigned turno)
{
   short int unsigned victoria = 0;
   short int unsigned fila, columna;
   short int unsigned i, j, contador;

   if (turno==3)
   {
      srand (time(NULL));
      turno=rand()%(2-1+1)+1;
      colocarBarcos(mm,mj);
   }

   while (victoria == 0)
   {
      system("cls");
      if (turno==1)
      {

           printf("\t\t\t\tTablero jugador\n");
       mostrarTablero(mj);
       printf("\t\t\t\tTablero Maquina\n");
          mostrarTablero(mm);
           printf("Elije una fila y una columna:\n");
       printf("Para guardar escribe 100\n");


       do
         {
            scanf("%hd", &fila);

            if(fila!=100) scanf("%hd", &columna);
             else
             {
                  guardarPartida(mm,mj);
                return 0;
             }
             fila--;
             columna--;

// se comprueba que la fila y columna elegida por el usuario corresponde a una posicion valida

            if (!(((fila >= 0 && fila < 7) && (columna >= 0 && columna < 7)) &&
                !(mm[columna][fila] == 1) && !(mm[columna][fila] == 2) &&
                !(mm[columna][fila] == 3)))
               printf("Valor incorrecto por favor elije una fila y una columna:\n");

         }
          while (!(((fila >= 0 && fila < 7) && (columna >= 0 && columna < 7)) &&
                !(mm[columna][fila] == 1) && !(mm[columna][fila] == 2) &&
                !(mm[columna][fila] == 3)));

// modifica devuelve 1 cuando se marca un barco hundido
// marca las posciones colindantes y cuenta los barcos hundidos

         if (modifica(mm,fila,columna))
         {
            marcarAlrededorDeBarcoHundido(mm ,fila, columna);

            contador = 0;
            for(i=0;i<7;i++)
               for(j=0;j<7;j++)
                  if(mm[i][j] == 3) contador++;
         }

         if (contador == 4) victoria = 1;
            else turno = 2;

      }

// maquina

      if(turno==2)
      {

         do
         {
            srand (time(NULL));
            fila = rand()%(7);
            columna = rand()%(7);
         }
         while(!( mj[columna][fila] == 0 || mj[columna][fila] == 4
               || mj[columna][fila] == 5));

         if (modifica(mj,fila,columna))
         {
            marcarAlrededorDeBarcoHundido(mj,fila, columna);

            contador = 0;
            for(i=0;i<7;i++)
               for(j=0;j<7;j++)
                  if(mj[i][j] == 3) contador++;
         }
         if (contador == 4)
            victoria = 2;
            else turno = 1;
      }
   }
   return victoria;
}

void colocarBarcos(short int unsigned mm[7][7],short int unsigned mj[7][7])
{
   short int unsigned  cont=0, VH;
   short int unsigned fila, columna;


   mostrarTablero(mm);
   mostrarTablero(mj);

   while(cont<2)
   {
      printf("\nColoquemos los barcos de -1- espacio:\n introduce fila y columna:\n");
      scanf("%hd%hd",&fila,&columna);
      fila = fila - 1;
      columna = columna -1;

      while (!((fila >= 0 && fila < 7) && (columna >= 0 && columna < 7)))
      {
         printf("\nColoca los barcos de -1-:\n introduce fila y columna Bien!-_-:\n");
         scanf("%hd%hd",&fila,&columna);
         fila = fila - 1;
         columna = columna -1;
      }

      if (!cerca(mj,fila,columna))
      {
         cont = cont + 1;
         mj[columna][fila] = 4;
      }

      system("cls");
      mostrarTablero(mj);
   }

   cont = 0;
   while(cont<2)
   {
      printf("\nColoquemos los barcos de -2- espacios:");
      printf("\n�Com� quieres que se coloquen?\n -Vetical [1] \n -Horizontal [2]\n");
      scanf("%hd", &VH);

      while (!(VH > 0 && VH < 3))
      {
         printf("\nColoquemos los barcos de -2- espacios:");
         printf("\n�Com� quieres que se coloquen?\n -Vetical [1] \n -Horizontal [2]\n");
         scanf("%hd", &VH);
      }

      printf("\nAhora introduce fila y columna\n:");
      scanf("%hd%hd",&fila,&columna);
      fila = fila - 1;
      columna = columna -1;
      /*
      VH = 1 -> evalua las posiciones en vertical
      VH = 2 -> evalua las posiciones en horizontal
      */
      if(VH == 1)
      {
         while (!((fila >= 0 && fila < 6) && (columna >= 0 && columna < 7)) || (cerca(mj,fila,columna) || cerca(mj,fila,columna+1)))
         {
            printf("\nColoca los barcos de -2-:\n introduce fila y columna Bien!^_^:\n");
            scanf("%hd%hd",&fila,&columna);
            fila = fila - 1;
            columna = columna -1;
         }
         cont = cont + 1;
         mj[columna][fila] = 5;
         mj[columna][fila+1] = 5;
      }
      else
      {
         while (!((fila >= 0 && fila < 7) && (columna >= 0 && columna < 6 )) || (cerca(mj,fila,columna) || cerca(mj,fila,columna+1)))
         {
            printf("\nColoca los barcos de -2-:\n introduce fila y columna Bien!^_^:\n");
            scanf("%hd%hd",&fila,&columna);
            fila = fila - 1;
            columna = columna -1;
         }
         cont = cont + 1;
         mj[columna][fila] = 5;
         mj[columna+1][fila] = 5;
      }

      system("cls");
      mostrarTablero(mj);
   }

// maquina

   cont = 0;
   while(cont < 2)
   {
      srand (time(NULL));
      fila = rand()%(7);
      columna = rand()%(7);

      if (!cerca(mm,fila,columna))
      {
         cont = cont + 1;
         mm[columna][fila] = 6;
      }
   }

   cont = 0;

   while(cont < 2)
   {
      srand (time(NULL));
      fila = rand()%(6);
      columna = rand()%(6);
      VH = rand()%2;
      if(VH == 1)
      {
         if (!cerca(mm,fila,columna))
            if(!cerca(mm ,fila+1,columna))
            {
               cont = cont + 1;
               mm[columna][fila] = 7;
               mm[columna][fila+1] = 7;
            }
      }
      else
      {
         if (!cerca(mm,fila,columna))
            if(!cerca(mm,fila,columna+1))
            {
               cont = cont + 1;
               mm[columna][fila] = 7;
               mm[columna+1][fila] = 7;
            }
      }
   }
   system("cls");
}
short int unsigned cerca(short int unsigned t[7][7],short int unsigned fila,short int unsigned columna)
{
   short int i,j;
   short int unsigned suma = 0;

   for(i = fila-1;i <= fila+1; i++ )
      for(j = columna-1; j <= columna+1; j++)
         if ( (j >= 0 && j < 7 ) && (i >= 0 && i < 7))
            suma = suma + t[j][i];

   if (suma == 0) return 0;
   else return 1;

}

void mostrarTablero(short int unsigned tablero[7][7])
{
   short int unsigned i, j;

   printf("\t");
   for (i = 1; i < 8; i++)
     printf("\t%d", i);
   printf("\n\n");

   for(i=0;i<7;i++)
   {
      for(j=0;j<8;j++)
      {
        if(j==0)
           printf("\t%d", i+1);
       else
       {
            if (tablero[j-1][i] == 0) printf("\t ");
            if (tablero[j-1][i] == 1) printf("\t*");
            if (tablero[j-1][i] == 2) printf("\t!");
            if (tablero[j-1][i] == 3) printf("\tO");
            if (tablero[j-1][i] == 4) printf("\t@");
            if (tablero[j-1][i] == 5) printf("\t@");
            if (tablero[j-1][i] == 6) printf("\t~");
            if (tablero[j-1][i] == 7) printf("\t~");
       }
     }
      printf("\n\n");
   }
}

short int unsigned modifica(short int unsigned t[7][7],short int unsigned fila,short int unsigned columna)
{
   short int i , j ;
   short int unsigned aux = 0;
   /*************************
   0 - agua
   1 - fallo
   2 - tocado
   3 - hundido
   4 - barcode1
   5 - barcode2
   6 - barcode1escondido
   7 - barcode2escondido
   *************************/
   if (t[columna][fila] == 0) t[columna][fila] = 1;

   if (t[columna][fila] == 4 || t[columna][fila] == 6)
   {
      t[columna][fila] = 3;
      aux = 1;
   }

   if (t[columna][fila] == 5 || t[columna][fila] == 7 )
   {
      for(i = fila-1;i <= fila+1; i++ )
         for(j = columna-1; j <= columna+1; j++)
            if ( (j >= 0 && j < 7 ) && (i >= 0 && i < 7))
               if (t[j][i] == 2)
               {
                  t[columna][fila] = 3;
                  aux = 1;
               }
        if (aux == 0) t[columna][fila] = 2;
   }

   return aux;

}

void marcarAlrededorDeBarcoHundido(short int unsigned t[7][7],short int unsigned fila,short int unsigned columna)
{
   short int i,k,p;
   short int vector[8][2];
   vector[0][0] =  -1; vector[0][1] =  -1;
   vector[1][0] =  -1; vector[1][1] =  0 ;
   vector[2][0] =  -1; vector[2][1] =  1 ;
   vector[3][0] =  0 ; vector[3][1] =  -1;
   vector[4][0] =  0 ; vector[4][1] =  1 ;
   vector[5][0] =  1 ; vector[5][1] =  -1;
   vector[6][0] =  1 ; vector[6][1] =  0 ;
   vector[7][0] =  1 ; vector[7][1] =  1 ;

   for (i = 0; i < 8; i++)
   {
      p = fila + vector[i][0];
      k = columna + vector[i][1];
      if (((k >= 0 && k < 7 ) && (p >= 0 && p < 7)))
      {
         if(t[k][p] == 0)
            t[k][p] = 1;
         else
            if (t[k][p] == 2)
               marcarAlrededorDeBarcoHundido(t,p,k);
      }

   }

}
   
