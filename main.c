/*
 * main.c
 *
 *  Created on: 20/3/2015
 *      Author: Damian Rivera Honores
 */
 
#include "cabecera.h"

int main()
{
   short int unsigned tableroMaquina[7][7]={0},tableroJugador[7][7]={0}, opcion, ganador;

   printf("Hundir la Flota!!\n");
   printf("Menu\n");
   printf("1.Cargar Partida\n");
   printf("2.Partida Nueva\n");
   scanf("%hd", &opcion);

   while(opcion!=2 && opcion!=1)
   {
      printf("Opcion incorrecta, elija una opcion...\n");
      scanf("%hd", &opcion);
   }

   system("cls");

   switch (opcion)
   {
      case 1 :
	     if(!cargarPartida(tableroMaquina,tableroJugador));
	        ganador = juego(tableroMaquina,tableroJugador,1);

	     system("cls");

	     if(ganador == 1)
         {
            printf("\nHas ganado!!");
            mostrarTablero(tableroMaquina);
 	     }
	        else
	        {
               printf("\nHas perdido...");
               mostrarTablero(tableroJugador);
            }

         break;

	  case 2 :
	     ganador = juego(tableroMaquina,tableroJugador,3);

		 system("cls");

 	     if(ganador == 1)
         {
            printf("\nHas ganado!!");
            mostrarTablero(tableroMaquina);
 	     }
	        else
	        {
               printf("\nHas perdido...");
               mostrarTablero(tableroJugador);
            }

	     break;
   }

   system("pause");

   return 0;

}

