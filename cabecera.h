/*
 * cabecera.h
 *
 *  Created on: 20/3/2015
 *      Author: Damian Rivera Honores
 */

#ifndef CABECERA_H_
#define CABECERA_H_

int cargarPartida(short int unsigned mm[7][7],short int unsigned mj[7][7]);

int guardarPartida(short int unsigned mm[7][7],short int unsigned mj[7][7]);

short int unsigned juego(short int unsigned mm[7][7],short int unsigned mj[7][7],short int unsigned turno);

void colocarBarcos(short int unsigned mm[7][7],short int unsigned mj[7][7]);

short int unsigned cerca(short int unsigned t[7][7],short int unsigned fila,short int unsigned columna);

void mostrarTablero(short int unsigned tablero[7][7]);

short int unsigned modifica(short int unsigned t[7][7],short int unsigned fila,short int unsigned columna);

void marcarAlrededorDeBarcoHundido(short int unsigned t[7][7],short int unsigned fila,short int unsigned columna);

#endif /* CABECERA_H_ */
